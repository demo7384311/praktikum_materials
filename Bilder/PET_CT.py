import nibabel as nib
import cv2
import SimpleITK as sitk
import numpy as np


# Normalization
def MaxMinNormalizer(data):
    data_max = np.max(data)
    data_min = np.min(data)
    data_normalize = (data - data_min) / (data_max - data_min)
    return data_normalize


# Fusion of CT data and PET data
def Fusion(path_CT, path_PET):
    image_CT = nib.load(path_CT).get_data()
    image_CT = image_CT.astype(float)
    image_CT_size = np.shape(image_CT)
    # Normalize CT
    image_CT = MaxMinNormalizer(image_CT)

    # Resampled whole-body PET image
    image_PET = nib.load(path_PET).get_data()
    image_PET = image_PET.astype(float)
    # Normalize PET
    image_PET = MaxMinNormalizer(image_PET)

    # Fusion ratio
    percent_list = []
    for i in range(1, 10):
        percent_list.append(i / 10)
    # Fusion CT, PET,
    # percent_list=[0.1] # best performance
    for percent in percent_list:
        ImageFusion = np.zeros(image_CT_size)
        for num in range(image_CT_size[2]):
            image_CT_slice = image_CT[:, :, num]
            image_PET_slice = image_PET[:, :, num]
            # Fusion data according to the ratio
            img_mix = cv2.addWeighted(image_CT_slice, percent, image_PET_slice, 1 - percent, 0)
            ImageFusion[:, :, num] = img_mix
        ImageFusion = np.transpose(ImageFusion, (2, 1, 0))
        ImageFusionISO = sitk.GetImageFromArray(ImageFusion, isVector=False)

        # Get image scan and spatial information
        input_PET_property = sitk.ReadImage(path_PET)
        spacing = np.array(input_PET_property.GetSpacing())
        direction = np.array(input_PET_property.GetDirection())
        Origin = np.array(input_PET_property.GetOrigin())

        # Reset the image information for the fused data
        ImageFusionISO.SetSpacing(spacing)
        ImageFusionISO.SetOrigin(Origin)
        ImageFusionISO.SetDirection(direction)

        # Save the resampled data
        soffix_name = path_PET[-7:]
        file_name = path_PET[:-19]
        # savepath_name = file_name + '_modified_fusioin_0.6' + soffix_name
        # savepath_name = file_name + 'CTPETFusioin_LINEAR_'+ str(percent) + soffix_name
        savepath_name = file_name + 'CTPETFusioin_' + str(percent) + soffix_name

        sitk.WriteImage(ImageFusionISO, savepath_name)

    pass


def main():
    name_list = ['CT', 'PET_Resample']
    root = '/data/'

    # Path to whole-body CT image data
    input_CT = root + name_list[0] + '.nii.gz'
    # Path to whole-body PET image data
    input_PET = root + name_list[1] + '.nii.gz'

    # Fuse CT and PET
    Fusion(input_CT, input_PET)


if __name__ == '__main__':
    main()
